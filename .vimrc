
set nocompatible                " be iMprovedH``JK```
filetype off                    " required!

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
" "call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'scrooloose/nerdtree'
Plugin 'Valloric/YouCompleteMe'
Plugin 'groenewege/vim-less'
Plugin 'closetag.vim'
Plugin 'scrooloose/syntastic'

" https://wakatime.com/help/plugins/vim
Plugin 'wakatime/vim-wakatime'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line


if $TMUX == ''
  set clipboard+=unnamed
endif

"设置语法高亮
syntax on

"现实行号
set number

" 高亮搜索
set hlsearch

"显示标尺
set ruler

" http://stackoverflow.com/questions/901313/editing-xml-files-with-long-lines-is-really-slow-in-vim-what-can-i-do-to-fix-th
set synmaxcol=200

set linebreak

"调整背景为深色背景
set background=dark

"使用配色方案
syntax enable
set background=dark
"let g:solarized_termcolors=256
colorscheme solarized

"用浅色高亮当前行
autocmd InsertLeave * se nocul
autocmd InsertEnter * se cul

"用空格填充制表符
set expandtab

"制表符长度是2个空格
set tabstop=2

"默认缩进是2个空格
set shiftwidth=2

" 自动缩进
"set autoindent

"设置字体
set guifont=Monospace\ 11

"设置折叠依据为缩进
set foldmethod=indent "syntax, indent

"标签页配置-通过ctrl-h/l切换前后标签
nnoremap <C-l> gt
nnoremap <C-h> gT

" key maps
" 双击 ESC 去除行尾空白字符然后保存
map <Esc><Esc> :%s/\s\+$//e<enter> :w<enter>

"nerdtree
nmap ,nt :NERDTreeToggle<CR>

" 高亮类html文件
au BufNewFile,BufRead *.ejs set filetype=html

" 如果打开的文件除了NERDTree没有其他文件时，它自动关闭
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") &&b:NERDTreeType == "primary") | q | endif

if has("win32")
  let $VIMFILES = $VIM.'/vimfiles'
else
  let $VIMFILES = $HOME.'/.vim'
endif

let g:vimwiki_list = [{'path': '~/vimwiki',
\    'path_html': '~/vimwiki_html/',
\    'template_path': '~/vimwiki/templates',
\    'template_default': 'default',
\    'template_ext': '.html'}]

let g:vimwiki_camel_case=0

" 全局设定 closetag 插件优先匹配 HTML 文件
let g:closetag_html_style=1

if !exists("b:unaryTagsStack") || exists("b:closetag_html_style")
    if &filetype == "html" || exists("b:closetag_html_style")
        let b:unaryTagsStacktack="area base br dd dt hr img input link meta param"
    else " for xml and xsl
        let b:unaryTagsStack=""
    endif
endif

if !exists("b:unaryTagsStack")
    let b:unaryTagsStack=""
endif

"高亮光标所在行和列
set cursorline
set cursorcolumn

" jshint
set runtimepath+=~/.vim/bundle/jshint2.vim/

" jshint validation
nnoremap <silent><F1> :JSHint<CR>
inoremap <silent><F1> <C-O>:JSHint<CR>
vnoremap <silent><F1> :JSHint<CR>

" show next jshint error
nnoremap <silent><F2> :lnext<CR>
inoremap <silent><F2> <C-O>:lnext<CR>
vnoremap <silent><F2> :lnext<CR>

" show previous jshint error
nnoremap <silent><F3> :lprevious<CR>
inoremap <silent><F3> <C-O>:lprevious<CR>
vnoremap <silent><F3> :lprevious<CR>


set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"let g:syntastic_always_populate_loc_list = 0
"let g:syntastic_auto_loc_list = 0

" 打开文件时即检查
" 默认是保存时才检查
let g:syntastic_check_on_open = 1

"let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers=['jscs', 'jshint']
"
"function! JavascriptLinter(curpath)
"  let parent=1
"  let local_path=a:curpath
"  let local_jscs=local_path . '.jscsrc'
"
"  while parent <= 255
"    let parent = parent + 1
"    if filereadable(local_jscs)
"      return ['jscs']
"    endif
"    let local_path = local_path . "../"
"    let local_jscs = local_path . '.jscsrc'
"  endwhile
"
"  unlet parent local_jscs
"
"  return ['jshint']
"endfunction
"let g:syntastic_javascript_checkers=JavascriptLinter(getcwd() . "/")
" OR
autocmd FileType javascript let b:syntastic_checkers = findfile('.jscsrc', '.;') != '' ? ['jscs'] : ['jshint']
